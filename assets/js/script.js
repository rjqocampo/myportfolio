function smoothScroll(target, duration) {
    var target = document.querySelector(target);
    var targetPosition = target.getBoundingClientRect().top;
    var startPosition = window.pageYOffset;
    var startTime = null;

    function animation(currentTime){
        if(startTime === null) startTime = currentTime;
        var timeElapsed = currentTime - startTime;
        var run = ease(timeElapsed, startPosition, targetPosition, duration);
        window.scrollTo(0, run);
        if(timeElapsed < duration) requestAnimationFrame(animation);
    }

    function ease (t, b, c, d) {
        t /= d/2;
        if (t < 1) return c/2*t*t + b;
        t--;
        return -c/2 * (t*(t-2) - 1) + b;
    };

    requestAnimationFrame(animation);
}

var homeButton = document.querySelectorAll(".nav-home");
var skillsButton = document.querySelectorAll('.nav-skills');
var projectsButton = document.querySelectorAll('.nav-projects');
var aboutButton = document.querySelectorAll('.nav-about');
var contactButton = document.querySelectorAll('.nav-contact');

homeButton.forEach(button => button.addEventListener('click', () => {
    smoothScroll('.main-container', 800);
}))

skillsButton.forEach(button => button.addEventListener('click', () => {
    smoothScroll('#section-2', 800);
}))

projectsButton.forEach(button => button.addEventListener('click', () => {
    smoothScroll('#section-3', 800);
}))

aboutButton.forEach(button => button.addEventListener('click', () => {
    smoothScroll('#section-4', 800);
}))

contactButton.forEach(button => button.addEventListener('click', () => {
    smoothScroll('#section-5', 800);
}))

var placeholder = document.querySelector(".btn-placeholder");
var gamebox = document.querySelector(".btn-gamebox");
var sherpa = document.querySelector(".btn-sherpa")

placeholder.addEventListener('click', () => {
    window.open("https://rjqocampo.gitlab.io/csp1/", "_blank");
});

gamebox.addEventListener('click', () => {
    window.open("https://gameboxgames.000webhostapp.com/menu", "_blank");
});

sherpa.addEventListener('click', () => {
    window.open("https://ocampo-csp3-frontend.herokuapp.com/", "_blank");
});

var gitlab = document.querySelector("#git-container");
var linkedin = document.querySelector("#linkedin-container");
var resume = document.querySelector("#resume-container");

gitlab.addEventListener('click', () => {
    setTimeout( () => {
        window.open("https://gitlab.com/rjqocampo", "_blank");
    }, 1000)
});

linkedin.addEventListener('click', () => {
    setTimeout( () => {
        window.open("https://www.linkedin.com/in/rod-john-ocampo-041041196/", "_blank");
    }, 1000)
});

resume.addEventListener('click', () => {
    setTimeout( () => {
        window.open("https://rjqocampo.gitlab.io/myportfolio/assets/pdf/RodJohn_Ocampo_Resume.pdf", "_blank");
    }, 1000)
});

